﻿var appData = appData || {};

appData.resourcesList =
[
    { "id": 1, "name": "Windows Server 2012 R2 Datacenter", "description": "Enterprise-class solutions that are simple to deploy, cost-effective, application-focused, and user-centric.", "iconUrl": "/app/img/datacenter.png" },
    { "id": 2, "name": "SQL Server 2014 AlwaysOn", "description": "Automates the deployment of a SQL Server AlwaysOn Availability Group for high availability of SQL Server workloads.", "iconUrl": "/app/img/sqlserver_alwayson.png" },
    { "id": 3, "name": "Ubuntu Server 14.04 LTS", "description": "Ubuntu Server delivers the best value scale-out performance available.", "iconUrl": "/app/img/ubuntu_server_14.png" },
    { "id": 4, "name": "SharePoint Server Farm", "description": "You can now easily deploy a highly available SharePoint server farm in Azure. This is designed to help you achieve new levels of reliability and performance, delivering features and capabilities that simplify administration, protect communications and information.", "iconUrl": "/app/img/sharepoint.png" },
    { "id": 5, "name": "Website", "description": "Enjoy secure and flexible development, deployment, and scaling options for your web app.", "iconUrl": "/app/img/websites.png" },
    { "id": 6, "name": "Search", "description": "Search-as-a-service solution", "iconUrl": "/app/img/search.png" },
    { "id": 7, "name": "DocumentDB", "description": "[Preview] Scalable and managed NoSQL document database service for modern cloud applications.", "iconUrl": "/app/img/documentdb.png" },
    { "id": 8, "name": "Team Project", "description": "Everything from Git repos and project tracking tools, to continuous integration and an IDE.", "iconUrl": "/app/img/team_project.png" },
    { "id": 9, "name": "SQL Database", "description": "Scalable and managed relational database service for modern business-class apps.", "iconUrl": "/app/img/sql_database.png" },
    { "id": 10, "name": "Application Insights", "description": "Application performance, availability and usage information at your fingertips.", "iconUrl": "/app/img/application_insights.png" },
    { "id": 11, "name": "Redis Cache", "description": "Distributed, in-memory Redis Cache service for modern cloud applications", "iconUrl": "/app/img/redis_cache.png" },
    { "id": 12, "name": "Website + SQL", "description": "Enjoy secure and flexible development, deployment, and scaling options for your web app plus a SQL database.", "iconUrl": "/app/img/website_sql.png" },
    { "id": 13, "name": "Storage", "description": "Enhance existing applications with durable cloud storage, backup, and recovery.", "iconUrl": "/app/img/storage.png" },
    { "id": 14, "name": "MySQL Database", "description": "A powerful, fault-tolerant database-as-a-service in the cloud for your MySQL powered applications.", "iconUrl": "/app/img/mysql.png" }
];

appData.repoList = [
    { "id": 1, "name": "plggd", "created_date": 1, "updated_date": 10 },
    { "id": 2, "name": "know", "created_date": 2, "updated_date": 9 },
    { "id": 3, "name": "skytrove", "created_date": 3, "updated_date": 8 },
    { "id": 4, "name": "launch", "created_date": 4, "updated_date": 7 },
    { "id": 5, "name": "azuremobile", "created_date": 5, "updated_date": 6 },
    { "id": 6, "name": "madhudj.com", "created_date": 6, "updated_date": 5 },
    { "id": 7, "name": "booom", "created_date": 7, "updated_date": 4 },
    { "id": 8, "name": "allangle", "created_date": 8, "updated_date": 3 },
    { "id": 9, "name": "fastmarch", "created_date": 9, "updated_date": 2 },
    { "id": 10, "name": "bohncompany", "created_date": 10, "updated_date": 1 }
];

appData.fullResourcesList =
[
    { "type": "Virtual Machines", "group": "Recommended", "by": "Microsoft", "name": "Windows Server 2012 R2 Datacenter", "iconUrl": "/App/img/large/virtual_machines/recommended/windows_server_2012_r2.png" },
    { "type": "Virtual Machines", "group": "Recommended", "by": "Canonical", "name": "Ubuntu Server 14.04 LTS", "iconUrl": "/App/img/large/virtual_machines/recommended/ubuntu_server_14.png" },
    { "type": "Virtual Machines", "group": "Recommended", "by": "Microsoft", "name": "SharePoint Server Farm", "iconUrl": "/App/img/large/virtual_machines/recommended/sharepoint_server_farm.png" },
    { "type": "Virtual Machines", "group": "Recommended", "by": "Microsoft", "name": "SQL Server 2014 AlwaysOn", "iconUrl": "/App/img/large/virtual_machines/recommended/sqlserver_alwayson.png" },
    { "type": "Virtual Machines", "group": "Recommended", "by": "OpenLogic", "name": "CentOS-based 6.5", "iconUrl": "/App/img/large/virtual_machines/recommended/centos_65.png" },
    { "type": "Virtual Machines", "group": "Recommended", "by": "Oracle", "name": "Oracle Database 12.1.0.1 Standard Edition", "iconUrl": "/App/img/large/virtual_machines/recommended/oracle_database_12_standard.png" },
    { "type": "Virtual Machines", "group": "Windows Based", "by": "Microsoft", "name": "SQL Server 2014 Standard", "iconUrl": "/App/img/large/virtual_machines/windows_based/windows_server_2012_r2.png" },
    { "type": "Virtual Machines", "group": "Windows Based", "by": "Microsoft", "name": "Windows Server 2012 R2 Datacenter", "iconUrl": "/App/img/large/virtual_machines/windows_based/sqlserver_2014_standard.png" },
    { "type": "Virtual Machines", "group": "Windows Based", "by": "Microsoft", "name": "SharePoint Server 2013 Trial", "iconUrl": "/App/img/large/virtual_machines/windows_based/sharepoint_server_2013_trial.png" },
    { "type": "Virtual Machines", "group": "Windows Based", "by": "MSOpenTech", "name": "Oracle Databased 12c Standard Edition", "iconUrl": "/App/img/large/virtual_machines/windows_based/oracle_database_112c_standard.png" },
    { "type": "Virtual Machines", "group": "Windows Based", "by": "Microsoft", "name": "BizTalk Server 2013 Enterprise Edition", "iconUrl": "/App/img/large/virtual_machines/windows_based/biztalk_server_2013_enterprise.png" },
    { "type": "Virtual Machines", "group": "Windows Based", "by": "MSOpenTech", "name": "Oracle Database 11g R2 and WebLogic Server 11g Enterprise Edition", "iconUrl": "/App/img/large/virtual_machines/windows_based/oracle_database_11g_r2.png" },
    { "type": "Virtual Machines", "group": "Linux based ", "by": "Canonical", "name": "Ubuntu Server 14.04 LTS", "iconUrl": "/App/img/large/virtual_machines/linux_based/ubunut_server_14.png" },
    { "type": "Virtual Machines", "group": "Linux based ", "by": "Oracle", "name": "Oracle Linux 6.4.0.0.0", "iconUrl": "/App/img/large/virtual_machines/linux_based/oracle_linux_64.png" },
    { "type": "Virtual Machines", "group": "Linux based ", "by": "SUSE", "name": "openSUSE 13.1", "iconUrl": "/App/img/large/virtual_machines/linux_based/opensuse_13.png" },
    { "type": "Virtual Machines", "group": "Linux based ", "by": "OpenLogic", "name": "CentOS-based 6.5", "iconUrl": "/App/img/large/virtual_machines/linux_based/centos_65.png" },
    { "type": "Virtual Machines", "group": "Linux based ", "by": "PuppetLabs", "name": "Puppet Enterprise 3.2.3", "iconUrl": "/App/img/large/virtual_machines/linux_based/puppet_enterprise.png" },
    { "type": "Virtual Machines", "group": "Linux based ", "by": "SUSE", "name": "SUSE Linux Enterprise Server", "iconUrl": "/App/img/large/virtual_machines/linux_based/suselinux_enterprise_server.png" },
    { "type": "Virtual Machines", "group": "VM depot", "by": "Bitnami", "name": "Ruby Stack 2.1.2-0-dev (Ubuntu 12.10)", "iconUrl": "/App/img/large/virtual_machines/vm_depot/ruby.png" },
    { "type": "Virtual Machines", "group": "VM depot", "by": "Basho", "name": "Riak", "iconUrl": "/App/img/large/virtual_machines/vm_depot/riak.png" },
    { "type": "Virtual Machines", "group": "VM depot", "by": "Bitnami", "name": "WordPress 3.9-0 (Ubuntu 12.10)", "iconUrl": "/App/img/large/virtual_machines/vm_depot/worpress_ubuntu.png" },
    { "type": "Virtual Machines", "group": "VM depot", "by": "GrahamDuerden", "name": "Jenkins", "iconUrl": "/App/img/large/virtual_machines/vm_depot/jenkins.png" },
    { "type": "Virtual Machines", "group": "VM depot", "by": "DaveCottlehuber", "name": "Apache CouchDB", "iconUrl": "/App/img/large/virtual_machines/vm_depot/couchdb.png" },
    { "type": "Virtual Machines", "group": "VM depot", "by": "Bitnami", "name": "Apache Solr 4.8.1-0 (Ubuntu 12.10)", "iconUrl": "/App/img/large/virtual_machines/vm_depot/solr.png" },
    { "type": "Virtual Machines", "group": "Azure Certified", "by": "Barracuda", "name": "Barracuda Web Application Firewall (WAF)", "iconUrl": "/App/img/large/virtual_machines/azure_certified/barracuda.png" },
    { "type": "Virtual Machines", "group": "Azure Certified", "by": "Bitnami", "name": "DreamFacotry 1.6", "iconUrl": "/App/img/large/virtual_machines/azure_certified/dreamfactory.png" },
    { "type": "Virtual Machines", "group": "Azure Certified", "by": "SAP", "name": "SAP HANA Developer Edition", "iconUrl": "/App/img/large/virtual_machines/azure_certified/sap_hana.png" },
    { "type": "Virtual Machines", "group": "Azure Certified", "by": "RiverbedTechnology", "name": "Riverbed SteelHead CX 8.6", "iconUrl": "/App/img/large/virtual_machines/azure_certified/riverbed_steelhead.png" },
    { "type": "Virtual Machines", "group": "Azure Certified", "by": "Oracle", "name": "Oracle Database 12.1.0.1 Enterprise Edition", "iconUrl": "/App/img/large/virtual_machines/azure_certified/oracle_db_12_ent.png" },
    { "type": "Virtual Machines", "group": "Azure Certified", "by": "MSOpenTech", "name": "Zulu OpenJDK package v1.7", "iconUrl": "/App/img/large/virtual_machines/azure_certified/zulu_openjdk.png" },
    { "type": "Virtual Machines", "group": "Database servers", "by": "Microsoft", "name": "SQL Server 2014 AlwaysOn", "iconUrl": "/App/img/large/virtual_machines/database_servers/sqlserver_2014_standard.png" },
    { "type": "Virtual Machines", "group": "Database servers", "by": "Microsoft", "name": "SQL Server 2012 SP1 Standard", "iconUrl": "/App/img/large/virtual_machines/database_servers/sqlserver_2012_standard.png" },
    { "type": "Virtual Machines", "group": "Database servers", "by": "Microsoft", "name": "SQL Server 2008 R2 SP2 Standard", "iconUrl": "/App/img/large/virtual_machines/database_servers/sqlserver_2008_standard.png" },
    { "type": "Virtual Machines", "group": "Database servers", "by": "Oracle", "name": "Oracle Database 12.1.0.1 Standard Edition", "iconUrl": "/App/img/large/virtual_machines/database_servers/oracle_database_12.png" },
    { "type": "Virtual Machines", "group": "Database servers", "by": "MSOpenTech", "name": "Oracle Database 12c Standard Edition", "iconUrl": "/App/img/large/virtual_machines/database_servers/oracle_database_12c.png" },
    { "type": "Virtual Machines", "group": "Application infrastructure", "by": "Microsoft", "name": "BizTalk Server 2013 Standard Edition", "iconUrl": "/App/img/large/virtual_machines/application_infrastructure/biztalk_server.png" },
    { "type": "Virtual Machines", "group": "Application infrastructure", "by": "MSOpenTech", "name": "Oracle WebLogic Server 12c Standard Edition", "iconUrl": "/App/img/large/virtual_machines/application_infrastructure/oracle_weblogic.png" },
    { "type": "Virtual Machines", "group": "Application infrastructure", "by": "SUSE", "name": "SUSE Linux Enterprise Server", "iconUrl": "/App/img/large/virtual_machines/application_infrastructure/suse_linux_ent.png" },
    { "type": "Virtual Machines", "group": "Application infrastructure", "by": "Microsoft", "name": "SharePoint Server Farm", "iconUrl": "/App/img/large/virtual_machines/application_infrastructure/sharepoint_serverfarm.png" },
    { "type": "Virtual Machines", "group": "Application infrastructure", "by": "Microsoft", "name": "Microsoft Dynamics GP 2013 Developer", "iconUrl": "/App/img/large/virtual_machines/application_infrastructure/microsoft_dynamics.png" },
    { "type": "Virtual Machines", "group": "Application infrastructure", "by": "MSOpenTech", "name": "Oracle Database 11g R2 and WebLogic Server 11g Enterprise Edition", "iconUrl": "/App/img/large/virtual_machines/application_infrastructure/oracle_db_11g.png" },
    { "type": "Virtual Machines", "group": "Networking", "by": "Microsoft", "name": "Virtual Network", "iconUrl": "/App/img/large/virtual_machines/networking/virtual_network.png" },
    { "type": "Virtual Machines", "group": "Networking", "by": "Microsoft", "name": "ExpressRouter", "iconUrl": "/App/img/large/virtual_machines/networking/expressroute.png" },
    { "type": "Virtual Machines", "group": "Networking", "by": "Microsoft", "name": "Traffic Manager", "iconUrl": "/App/img/large/virtual_machines/networking/traffic_manager.png" },
    { "type": "Virtual Machines", "group": "Networking", "by": "Microsoft", "name": "CDN", "iconUrl": "/App/img/large/virtual_machines/networking/cdn.png" },
    { "type": "Virtual Machines", "group": "Add-ons", "by": "Microsoft", "name": "Application Insights", "iconUrl": "/App/img/large/virtual_machines/add_ons/application_insights.png" },
    { "type": "Virtual Machines", "group": "Add-ons", "by": "New Relic, Inc.", "name": "New Relic", "iconUrl": "/App/img/large/virtual_machines/add_ons/new_relic.png" },
    { "type": "Virtual Machines", "group": "Add-ons", "by": "AppDynamics", "name": "AppDynamics", "iconUrl": "/App/img/large/virtual_machines/add_ons/app_dynamics.png" },
    { "type": "Virtual Machines", "group": "Add-ons", "by": "ClearPointe", "name": "ClearPointe Azure Management", "iconUrl": "/App/img/large/virtual_machines/add_ons/clearpointe_azure_management.png" },
    { "type": "Virtual Machines", "group": "Add-ons", "by": "Microsoft", "name": "Active Directory", "iconUrl": "/App/img/large/virtual_machines/add_ons/active_directory.png" },
    { "type": "Virtual Machines", "group": "Add-ons", "by": "Microsoft", "name": "SQL Database", "iconUrl": "/App/img/large/virtual_machines/add_ons/sql_db.png" },
    { "type": "Web", "group": "Recommended", "by": "Microsoft", "name": "Website", "iconUrl": "/App/img/large/web/recommended/website.png" },
    { "type": "Web", "group": "Recommended", "by": "Microsoft", "name": "Website + SQL", "iconUrl": "/App/img/large/web/recommended/website_sql.png" },
    { "type": "Web", "group": "Recommended", "by": "Microsoft", "name": "Website + MySQL", "iconUrl": "/App/img/large/web/recommended/website_mysql.png" },
    { "type": "Web", "group": "Recommended", "by": "WordPress", "name": "Scalable WordPress", "iconUrl": "/App/img/large/web/recommended/scalable_worpress.png" },
    { "type": "Web", "group": "Recommended", "by": "umbraco.org", "name": "Umbraco CMS", "iconUrl": "/App/img/large/web/recommended/umbraco_cms.png" },
    { "type": "Web", "group": "Recommended", "by": "DNN Corp", "name": "DNN Platform", "iconUrl": "/App/img/large/web/recommended/dnn_platform.png" },
    { "type": "Web", "group": "Blog + CMSs", "by": "Composite", "name": ".NET CMS Composite C1", "iconUrl": "/App/img/large/web/blogs_cms/dotnet_cms.png" },
    { "type": "Web", "group": "Blog + CMSs", "by": "Joomla", "name": "Joomla!", "iconUrl": "/App/img/large/web/blogs_cms/joomla.png" },
    { "type": "Web", "group": "Blog + CMSs", "by": "Acquia.com", "name": "Acquia Drupal 7 on SQL", "iconUrl": "/App/img/large/web/blogs_cms/drupal7_sql.png" },
    { "type": "Web", "group": "Blog + CMSs", "by": "Devbridge Group", "name": "Better CMS", "iconUrl": "/App/img/large/web/blogs_cms/better_cms.png" },
    { "type": "Web", "group": "Blog + CMSs", "by": "Kentico8", "name": "Kentico 8.1", "iconUrl": "/App/img/large/web/blogs_cms/kentico_81.png" },
    { "type": "Web", "group": "Blog + CMSs", "by": "Mono Software", "name": "MonoX", "iconUrl": "/App/img/large/web/blogs_cms/monox.png" },
    { "type": "Web", "group": "Starter sites", "by": "Microsoft", "name": "ASP.NET Empty Site", "iconUrl": "/App/img/large/web/starter_sites/aspnet_starter_site.png" },
    { "type": "Web", "group": "Starter sites", "by": "Microsoft", "name": "ASP.NET Starter Site", "iconUrl": "/App/img/large/web/starter_sites/aspnet_empty_site.png" },
    { "type": "Web", "group": "Starter sites", "by": "Microsoft", "name": "HTML5 Empty Site", "iconUrl": "/App/img/large/web/starter_sites/html5_empty_site.png" },
    { "type": "Web", "group": "Starter sites", "by": "Justin Beckwith", "name": "Node JS Starter Site", "iconUrl": "/App/img/large/web/starter_sites/express.png" },
    { "type": "Web", "group": "Starter sites", "by": "Justin Beckwith", "name": "Express Site", "iconUrl": "/App/img/large/web/starter_sites/nodejs_starter_site.png" },
    { "type": "Web", "group": "Starter sites", "by": "Microsoft", "name": "PHP Empty Site", "iconUrl": "/App/img/large/web/starter_sites/php_empty_site.png" },
    { "type": "Web", "group": "App Frameworks", "by": "PTVS", "name": "Django", "iconUrl": "/App/img/large/web/app_frameworks/django.png" },
    { "type": "Web", "group": "App Frameworks", "by": "Cake Software Foundation", "name": "CakePHP", "iconUrl": "/App/img/large/web/app_frameworks/cakephp.png" },
    { "type": "Web", "group": "App Frameworks", "by": "PTVS", "name": "Flask", "iconUrl": "/App/img/large/web/app_frameworks/flask.png" },
    { "type": "Web", "group": "App Frameworks", "by": "PTVS", "name": "Bottle", "iconUrl": "/App/img/large/web/app_frameworks/bottle.png" },
    { "type": "Web", "group": "App Frameworks", "by": "Microsoft", "name": "Apache Tomcat 7", "iconUrl": "/App/img/large/web/app_frameworks/apache_tomcat7.png" },
    { "type": "Web", "group": "App Frameworks", "by": "Microsoft", "name": "Jetty", "iconUrl": "/App/img/large/web/app_frameworks/jetty.png" },
    { "type": "Web", "group": "Ecommerce", "by": "EC-CUBE", "name": "EC-CUBE", "iconUrl": "/App/img/large/web/ecommerce/ec_cube.png" },
    { "type": "Web", "group": "Ecommerce", "by": "nopCommerce", "name": "nopCommerce", "iconUrl": "/App/img/large/web/ecommerce/nop_commerce.png" },
    { "type": "Web", "group": "Ecommerce", "by": "Vitro Commerce", "name": "Vitro Commerce", "iconUrl": "/App/img/large/web/ecommerce/virto_commerce.png" },
    { "type": "Web", "group": "Ecommerce", "by": "Commerce Guys", "name": "Drupal Commerce Kickstart on MySQL", "iconUrl": "/App/img/large/web/ecommerce/drupal_commer_on_sql.png" },
    { "type": "Web", "group": "Ecommerce", "by": "Commerce Guys", "name": "Drupal Commerce Kickstart on SQL", "iconUrl": "/App/img/large/web/ecommerce/drupal_commer_on_mysql.png" },
    { "type": "Web", "group": "Ecommerce", "by": "Standing Cloud, Inc.", "name": "OpenX", "iconUrl": "/App/img/large/web/ecommerce/openx.png" },
    { "type": "Web", "group": "Other", "by": "Hallo Welt! - Medienwerkstatt GmbH", "name": "MediaWiki", "iconUrl": "/App/img/large/web/other/mediawiki.png" },
    { "type": "Web", "group": "Other", "by": "bugenetproject", "name": "BugNET", "iconUrl": "/App/img/large/web/other/bugnet.png" },
    { "type": "Web", "group": "Other", "by": "Avensoft", "name": "nService", "iconUrl": "/App/img/large/web/other/nservice.png" },
    { "type": "Web", "group": "Other", "by": "Tech Info Systems", "name": "Galley Server Pro", "iconUrl": "/App/img/large/web/other/galler_server_pro.png" },
    { "type": "Web", "group": "Other", "by": "Mindroute Incentive", "name": "Incentive", "iconUrl": "/App/img/large/web/other/incentive.png" },
    { "type": "Web", "group": "Other", "by": "phpBB", "name": "phpBB on SQL", "iconUrl": "/App/img/large/web/other/phpbb_on_sql.png" },
    { "type": "Web", "group": "Add-ons", "by": "Microsoft", "name": "Application Insights", "iconUrl": "/App/img/large/web/add_ons/application_insights.png" },
    { "type": "Web", "group": "Add-ons", "by": "New Relic, Inc.", "name": "New Relic", "iconUrl": "/App/img/large/web/add_ons/new_relic.png" },
    { "type": "Web", "group": "Add-ons", "by": "AppDynamics", "name": "AppDynamics", "iconUrl": "/App/img/large/web/add_ons/appdynamics.png" },
    { "type": "Web", "group": "Add-ons", "by": "SendGrid", "name": "SendGrid", "iconUrl": "/App/img/large/web/add_ons/sendgrid.png" },
    { "type": "Web", "group": "Add-ons", "by": "ObjectLabs Corporation", "name": "MongoLab", "iconUrl": "/App/img/large/web/add_ons/mongolab.png" },
    { "type": "Web", "group": "Add-ons", "by": "Microsoft", "name": "Schedule", "iconUrl": "/App/img/large/web/add_ons/scheduler.png" },
    { "type": "Mobile", "group": "Recommended", "by": "Microsoft", "name": "Mobile Service", "iconUrl": "/App/img/large/mobile/recommended/mobile_service.png" },
    { "type": "Mobile", "group": "Recommended", "by": "Microsoft", "name": "Notification Hub", "iconUrl": "/App/img/large/mobile/recommended/notification_hub.png" },
    { "type": "Mobile", "group": "Recommended", "by": "Microsoft", "name": "Scheduler", "iconUrl": "/App/img/large/mobile/recommended/scheduler.png" },
    { "type": "Mobile", "group": "Add-ons", "by": "SendGrid", "name": "SendGrid", "iconUrl": "/App/img/large/mobile/add_ons/send_grid.png" },
    { "type": "Mobile", "group": "Add-ons", "by": "Aditi Technologies", "name": "Scheduler", "iconUrl": "/App/img/large/mobile/add_ons/aditi_scheduler.png" },
    { "type": "Mobile", "group": "Add-ons", "by": "Pusher", "name": "Pusher", "iconUrl": "/App/img/large/mobile/add_ons/pusher.png" },
    { "type": "Mobile", "group": "Add-ons", "by": "Buddy Platform", "name": "Buddy", "iconUrl": "/App/img/large/mobile/add_ons/buddy.png" },
    { "type": "Mobile", "group": "Add-ons", "by": "PubNub Inc.", "name": "PubNub", "iconUrl": "/App/img/large/mobile/add_ons/pubnub.png" },
    { "type": "Mobile", "group": "Add-ons", "by": "Microsoft", "name": "Service Bus", "iconUrl": "/App/img/large/mobile/add_ons/service_bus.png" },
    { "type": "Developer services", "group": "Recommended", "by": "Microsoft", "name": "Team Project", "iconUrl": "/App/img/large/developer_services/recommeded/teamproject.png" },
    { "type": "Developer services", "group": "Recommended", "by": "Microsoft", "name": "Application Insights", "iconUrl": "/App/img/large/developer_services/recommeded/application_insights.png" },
    { "type": "Developer services", "group": "Recommended", "by": "Microsoft", "name": "Visual Studio Ultimate 2013 on Windows Server 2012", "iconUrl": "/App/img/large/developer_services/recommeded/vs2013_windows2012.png" },
    { "type": "Developer services", "group": "Monitoring + diagnositics", "by": "Microsoft", "name": "Application Insights", "iconUrl": "/App/img/large/developer_services/monitoring_diagnostics/application_insights.png" },
    { "type": "Developer services", "group": "Monitoring + diagnositics", "by": "New Relic, Inc.", "name": "New Relic", "iconUrl": "/App/img/large/developer_services/monitoring_diagnostics/newrelic.png" },
    { "type": "Developer services", "group": "Monitoring + diagnositics", "by": "ClearPointe", "name": "ClearPointe Azure Management", "iconUrl": "/App/img/large/developer_services/monitoring_diagnostics/clearpoint_azure_management.png" },
    { "type": "Developer services", "group": "Monitoring + diagnositics", "by": "Alert Logic Inc.", "name": "Alert Logic Log Manager", "iconUrl": "/App/img/large/developer_services/monitoring_diagnostics/alert_logic_log.png" },
    { "type": "Developer services", "group": "Monitoring + diagnositics", "by": "AppDynamics", "name": "AppDynamics", "iconUrl": "/App/img/large/developer_services/monitoring_diagnostics/appdynamics.png" },
    { "type": "Developer services", "group": "Monitoring + diagnositics", "by": "Logentries", "name": "Logentries", "iconUrl": "/App/img/large/developer_services/monitoring_diagnostics/logentries.png" },
    { "type": "Developer services", "group": "Dev + test tools", "by": "SendGrid", "name": "loader io", "iconUrl": "/App/img/large/developer_services/devtools/loaderio.png" },
    { "type": "Developer services", "group": "Dev + test tools", "by": "Spirent Communications", "name": "Blitz Load Testing", "iconUrl": "/App/img/large/developer_services/devtools/blitz_load_testing.png" },
    { "type": "Developer services", "group": "Dev + test tools", "by": "Microsoft", "name": "Team Project", "iconUrl": "/App/img/large/developer_services/devtools/team_project.png" },
    { "type": "Developer services", "group": "Dev + test tools", "by": "Microsoft", "name": "Automation", "iconUrl": "/App/img/large/developer_services/devtools/automation.png" },
    { "type": "Developer services", "group": "Dev + test tools", "by": "Constellation Technologies S.L.", "name": "VS Anywhere", "iconUrl": "/App/img/large/developer_services/devtools/vs_anywhere.png" },
    { "type": "Media", "group": "Recommended", "by": "Microsoft", "name": "Media Service", "iconUrl": "/App/img/large/media/recommended/media_service.png" },
    { "type": "Media", "group": "Recommended", "by": "Microsoft", "name": "Storage", "iconUrl": "/App/img/large/media/recommended/storage.png" },
    { "type": "Media", "group": "Recommended", "by": "Microsoft", "name": "CDN", "iconUrl": "/App/img/large/media/recommended/cdn.png" },
    { "type": "Media", "group": "Add-ons", "by": "EZDRM", "name": "EZDRM Hosted PlayReady DRM", "iconUrl": "/App/img/large/media/add_ons/ezdrm_playready.png" },
    { "type": "Media", "group": "Add-ons", "by": "Digital Rapids", "name": "Kayak Media Processors", "iconUrl": "/App/img/large/media/add_ons/kayak_media_processor.png" },
    { "type": "Media", "group": "Add-ons", "by": "Aspera", "name": "Aspera Server On Demand", "iconUrl": "/App/img/large/media/add_ons/aspera_server.png" },
    { "type": "Media", "group": "Add-ons", "by": "Unixon Systems", "name": "STORM Manager File Tranfer System", "iconUrl": "/App/img/large/media/add_ons/storm_manager_filetransfer.png" },
    { "type": "Media", "group": "Add-ons", "by": "Bitline", "name": "Bitline", "iconUrl": "/App/img/large/media/add_ons/bitline.png" },
    { "type": "Data storage, cache + backup", "group": "Data services", "by": "Microsoft", "name": "SQL Database", "iconUrl": "/App/img/large/backup/data_services/sql.png" },
    { "type": "Data storage, cache + backup", "group": "Data services", "by": "Microsoft", "name": "DocumentDB", "iconUrl": "/App/img/large/backup/data_services/document_db.png" },
    { "type": "Data storage, cache + backup", "group": "Data services", "by": "Microsoft", "name": "Storage", "iconUrl": "/App/img/large/backup/data_services/storage.png" },
    { "type": "Data storage, cache + backup", "group": "Data services", "by": "Microsoft", "name": "Redis Cache", "iconUrl": "/App/img/large/backup/data_services/redis_cache.png" },
    { "type": "Data storage, cache + backup", "group": "Data services", "by": "Microsoft", "name": "Search", "iconUrl": "/App/img/large/backup/data_services/search.png" },
    { "type": "Data storage, cache + backup", "group": "Data services", "by": "Microsoft", "name": "HDInsight", "iconUrl": "/App/img/large/backup/data_services/hdinsights.png" },
    { "type": "Data storage, cache + backup", "group": "Database servers", "by": "Microsoft", "name": "SQL Server 2014 AlwaysOn", "iconUrl": "/App/img/large/backup/database_servers/sqlserver_alwayson.png" },
    { "type": "Data storage, cache + backup", "group": "Database servers", "by": "Microsoft", "name": "SQL Server 2014 Standard", "iconUrl": "/App/img/large/backup/database_servers/sqlserver_2008_standard.png" },
    { "type": "Data storage, cache + backup", "group": "Database servers", "by": "Microsoft", "name": "SQL Server 2012 SP1 Standard", "iconUrl": "/App/img/large/backup/database_servers/sqlserver_2012_standard.png" },
    { "type": "Data storage, cache + backup", "group": "Database servers", "by": "Microsoft", "name": "SQL Server 2008 R2 SP2 Standard", "iconUrl": "/App/img/large/backup/database_servers/sqlserver_2014_standard.png" },
    { "type": "Data storage, cache + backup", "group": "Database servers", "by": "Oracle", "name": "Oracle Database 12.1.0.1 Standard Edition", "iconUrl": "/App/img/large/backup/database_servers/oracle_database_12.png" },
    { "type": "Data storage, cache + backup", "group": "Database servers", "by": "MSOpenTech", "name": "Oracle Database 12c Standard Edition", "iconUrl": "/App/img/large/backup/database_servers/oracle_database_12c.png" },
    { "type": "Data storage, cache + backup", "group": "Backup", "by": "Microsoft", "name": "Backup", "iconUrl": "/App/img/large/backup/backup/backup.png" },
    { "type": "Data storage, cache + backup", "group": "Backup", "by": "Microsoft", "name": "Hyper-V Recovery Manager", "iconUrl": "/App/img/large/backup/backup/hyperv_recovery.png" },
    { "type": "Data storage, cache + backup", "group": "Backup", "by": "Cloud Cellar LLC", "name": "Automatic Backup", "iconUrl": "/App/img/large/backup/backup/automatic_backup.png" },
    { "type": "Identity", "group": "Recommended", "by": "Microsoft", "name": "Active Directory", "iconUrl": "/App/img/large/identity/recommended/active_directory.png" },
    { "type": "Identity", "group": "Recommended", "by": "Microsoft", "name": "Multi-Factor Authentication", "iconUrl": "/App/img/large/identity/recommended/multi_factor_auth.png" },
    { "type": "Identity", "group": "Recommended", "by": "Microsoft", "name": "Windows Server 2012 R2 Datacenter", "iconUrl": "/App/img/large/identity/recommended/windows_server_2012_r2.png" },
    { "type": "Identity", "group": "Add-ons", "by": "Auth0", "name": "Auth0", "iconUrl": "/App/img/large/identity/add_ons/auth0.png" },
    { "type": "App + data services", "group": "Integeration", "by": "Microsoft", "name": "BizTalk Service", "iconUrl": "/App/img/large/appndata/integration/biztalk_service.png" },
    { "type": "App + data services", "group": "Integeration", "by": "Microsoft", "name": "BizTalk Server 2013 Enterprise Edition", "iconUrl": "/App/img/large/appndata/integration/biztalk_ent.png" },
    { "type": "App + data services", "group": "Integeration", "by": "Microsoft", "name": "Service Bus", "iconUrl": "/App/img/large/appndata/integration/service_bus.png" },
    { "type": "App + data services", "group": "Integeration", "by": "Microsoft", "name": "Notification Hub", "iconUrl": "/App/img/large/appndata/integration/notification_hub.png" },
    { "type": "App + data services", "group": "Integeration", "by": "Pusher", "name": "Pusher", "iconUrl": "/App/img/large/appndata/integration/pusher.png" },
    { "type": "App + data services", "group": "Integeration", "by": "SendGrid", "name": "SendGrid", "iconUrl": "/App/img/large/appndata/integration/sendgrid.png" },
    { "type": "App + data services", "group": "Data insights", "by": "Microsoft", "name": "Microsoft Translator", "iconUrl": "/App/img/large/appndata/data_insights/microsoft_translator.png" },
    { "type": "App + data services", "group": "Data insights", "by": "Weather Services International (WSI)", "name": "Super MicroCast Forecast Data", "iconUrl": "/App/img/large/appndata/data_insights/wsi_super_microcast_forecast.png" },
    { "type": "App + data services", "group": "Data insights", "by": "Microsoft", "name": "Bing Search API", "iconUrl": "/App/img/large/appndata/data_insights/bing_search_api.png" },
    { "type": "App + data services", "group": "Data insights", "by": "D&B", "name": "D&B Business Insight", "iconUrl": "/App/img/large/appndata/data_insights/dnb_business_insight.png" },
    { "type": "App + data services", "group": "Data insights", "by": "Embarke", "name": "Embarke Email Analytics", "iconUrl": "/App/img/large/appndata/data_insights/embarke_email_anlaytics.png" },
    { "type": "App + data services", "group": "Data insights", "by": "Weather Trends International", "name": "Worldwide Historical Weather Data", "iconUrl": "/App/img/large/appndata/data_insights/worldwide_historical_weather.png" },
    { "type": "App + data services", "group": "Data Validation", "by": "Melissa Data Corporation", "name": "Address Check", "iconUrl": "/App/img/large/appndata/data_validations/melissa_data.png" },
    { "type": "App + data services", "group": "Data Validation", "by": "StrikeIron", "name": "Sales and Use Tax Rates Complete", "iconUrl": "/App/img/large/appndata/data_validations/sales_use_taxrates.png" },
    { "type": "App + data services", "group": "Data Validation", "by": "Melissa Data Corporation", "name": "IP Check", "iconUrl": "/App/img/large/appndata/data_validations/ip_check.png" },
    { "type": "App + data services", "group": "Data Validation", "by": "StrikeIron", "name": "Phone Number Validation", "iconUrl": "/App/img/large/appndata/data_validations/strikeiron_phonenumbervalidation.png" },
    { "type": "App + data services", "group": "Data Validation", "by": "Loqate", "name": "Verify", "iconUrl": "/App/img/large/appndata/data_validations/loqate_verify.png" },
    { "type": "App + data services", "group": "Content Services", "by": "ABBYY", "name": "ABBYY Cloud OCR SDK Service", "iconUrl": "/App/img/large/appndata/content_services/abbyy_cloud.png" },
    { "type": "App + data services", "group": "Content Services", "by": "Bitline", "name": "Bitline", "iconUrl": "/App/img/large/appndata/content_services/butline.png" },
    { "type": "App + data services", "group": "Content Services", "by": "Cloudinary Ltd.", "name": "cloudinary", "iconUrl": "/App/img/large/appndata/content_services/cloudindary.png" }
];