﻿var app = angular.module('azuremobile', ['ionic']);

app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('portal', {
            url: "/portal",
            abstract: true,
            templateUrl: "app/sidemenu.html"
        })
        .state('portal.home', {
            url: "/home",
            views: {
                'menuContent': {
                    templateUrl: "app/home.html",
                    controller: "homeController"
                }
            }
        })
        .state('portal.notifications', {
            url: "/notifications",
            views: {
                'menuContent': {
                    templateUrl: "app/notifications.html"
                }
            }
        })
        .state('portal.browse', {
            url: "/browse",
            views: {
                'menuContent': {
                    templateUrl: "app/browse.html"
                }
            }
        })
        .state('portal.journeys', {
            url: "/journeys",
            views: {
                'menuContent': {
                    templateUrl: "app/journeys.html"
                }
            }
        })
                .state('portal.website', {
                    url: "/website",
                    views: {
                        'menuContent': {
                            templateUrl: "app/journeys.website.html"
                        }
                    }
                })

        .state('portal.addwebsite', {
            url: "/addwebsite",
            views: {
                'menuContent': {
                    templateUrl: "app/add.website.html"
                }
            }
        })

        .state('portal.addwebsite2', {
            url: "/addwebsite2",
            views: {
                'menuContent': {
                    templateUrl: "app/add.website2.html"
                }
            }
        })

        .state('portal.billing', {
            url: "/billing",
            views: {
                'menuContent': {
                    templateUrl: "app/billing.html",
                    //controller: "billingCtrl"
                }
            }
        })
        .state('portal.new', {
            url: "/new",
            views: {
                'menuContent': {
                    templateUrl: "app/new.html",
                    //controller: "newController"
                }
            }
        });

    $urlRouterProvider.otherwise("/portal/home");
});

app.directive('shellIframeOnload', [function () {
    return {
        scope: {
            callBack: '&shellIframeOnload'
        },
        link: function (scope, element, attrs) {
            element.on('load', function () {
                scope.ready = true;
                return scope.callBack();
            });
        }
    }
}
]);

app.controller("startController", ['$scope',
    function startController($scope) {
        $scope.didIframeLoadComplete = function (cardItem) {
            $scope.$apply(function () {
                $scope.appReady = true;
            });
        }
    }
]);

app.controller("mainController", ['$scope', '$rootScope', '$window', '$timeout', '$ionicSideMenuDelegate',
    function mainController($scope, $rootScope, $window, $timeout, $ionicSideMenuDelegate) {
        $scope.state = {
            currentView: 0,
            currentSection: 0
        };

        $rootScope.contextMenu = {
            currentType: '',
            currentGroup: ''
        };

        $rootScope.memory = {
            newItemHistory: [],
            browseItemHistory: []
        };

        $rootScope.firstRun = true;

        $scope.toggleLeft = function () {
            $ionicSideMenuDelegate.toggleLeft();
        };

        //$scope.didSideMenuItemClick = function (actionItem) {
        //    console.log(actionItem);
        //}

        $scope.goBack = function () {
            $window.history.back();
        }

        $scope.didSideMenuItemClick = function () {
            $rootScope.firstRun = false;
            $rootScope.websiteName = '';
        }

        $timeout(function () {
            if (!$rootScope.firstRun) return;
            $ionicSideMenuDelegate.toggleLeft(true);
        }, 5000);
    }
]);

app.controller("homeController", ['$scope', '$rootScope', '$ionicSideMenuDelegate',
    function homeController($scope, $rootScope, $ionicSideMenuDelegate) {
        $scope.state.currentView = 1;

        $rootScope.didResourceFilterClick = function (id) {
            alert("From home " + id);
        }
    }
]);


app.controller("newController", ['$scope', '$rootScope', '$timeout', '$ionicSideMenuDelegate', '$ionicModal', '$ionicTabsDelegate', '$state',
    function newController($scope, $rootScope, $timeout, $ionicSideMenuDelegate, $ionicModal, $ionicTabsDelegate, $state) {
        $scope.state.currentView = 5;
        $scope.state.currentSection = 1;
        function _init() {
            $scope.data = {
                resourcesList: appData.resourcesList,
                searchState: '',
                repoList: appData.repoList
            }
            $scope.contextMenu = { currentType: '', currentGroup: '' };
            $scope.flag = {
                showEverything: false,
                searchVisible: false
            }
            $rootScope.flag = $rootScope.flag || {};
            $rootScope.flag.searchVisible = false;
            $scope.repoSortBy = "created_date";
        };
        _init();

        $timeout(function () {
            $ionicTabsDelegate.select(2);
        });

        $rootScope.didResourceFilterClick = function (id) {
            $scope.state.currentSection = id;
            if (id == 1) {
                $scope.flag.showEverything = false;
                $scope.closeModal();
            }
            if (id == 99) {
                $scope.flag.showEverything = true;
                $scope.contextMenu.currentType = "";
                $scope.contextMenu.currentGroup = "";
                $scope.data.fullResourcesList = angular.copy(appData.fullResourcesList);
                $scope.contextMenu.resourceTypes = _.keys(_.groupBy($scope.data.fullResourcesList, function (resource) { return resource.type; }));
            }
            $ionicSideMenuDelegate.toggleRight(false);
        }

        $rootScope.didRepoFilterSelectClick = function (id) {
            console.log(id);
            id == 1 ? $scope.repoSortBy = "created_date" : $scope.repoSortBy = "updated_date";
            console.log($scope.repoSortBy);
            $scope.closeRepoModal();
        }

        $scope.didResourceTypeSelelct = function (resourceType) {
            $scope.flag.showEverything = true;
            $scope.contextMenu.currentType = resourceType;
            $scope.data.fullResourcesList = _.where(angular.copy(appData.fullResourcesList), { type: resourceType });
            $scope.contextMenu.resourceGroups = _.keys(_.groupBy($scope.data.fullResourcesList, function (resource) { return resource.group; }));
            $ionicSideMenuDelegate.toggleRight(false);
            $scope.state.currentSection = 2;
            $scope.closeModal();
        }

        $scope.didResourceGroupSelelct = function (resourceGroup) {
            $scope.flag.showEverything = true;
            $scope.contextMenu.currentGroup = resourceGroup;
            $scope.data.fullResourcesList = _.where(angular.copy(appData.fullResourcesList), { type: $scope.contextMenu.currentType, group: resourceGroup });
            //$rootScope.contextMenu.resourceGroups = _.keys(_.groupBy($scope.data.fullResourcesList, function (resource) { return resource.group; }));
            $ionicSideMenuDelegate.toggleRight(false);
            $scope.state.currentSection = 2;
            $scope.closeModal();
        }

        $scope.didViewToggleClick = function () {
            //if ($scope.flag.listView) { // Changing to Grid View
            //    if (!$scope.data.gridData) {
            //        $scope.data.gridData = [];
            //        var colsCount = 1;
            //        var row = [];
            //        angular.forEach(appData.resourcesList, function (resource) {
            //            console.log(resource);
            //            if (row.length == 4) {
            //                $scope.data.gridData.push(row);
            //                row = [];
            //            }
            //            row.push(resource);
            //            console.log(row);
            //            colsCount++;
            //        });
            //        var n = row.length;
            //        for (var i = 0; i < 4 - n; i++) {
            //            row.push(false);
            //        }
            //        $scope.data.gridData.push(row);
            //        console.log($scope.data.gridData);
            //    }
            //}

            //$scope.flag.listView = !$scope.flag.listView;
        }

        $scope.didAddNewResourceClick = function (resource) {
            var index = $rootScope.memory.newItemHistory.indexOf(resource);
            if (index == -1) {
                $rootScope.memory.newItemHistory.push(resource);
            }
            if (resource.name == "Website") {
                $rootScope.websiteName = '';
                $state.go('portal.addwebsite');
                console.log('Adding website');
            }
            console.log('Selected resource is ', resource);
        }

        $scope.didBrowseResourceClick = function (resource) {
            var index = $rootScope.memory.browseItemHistory.indexOf(resource);
            if (index == -1) {
                $rootScope.memory.browseItemHistory.push(resource);
            }
            console.log('Selected resource is ', resource);
        }


        $ionicModal.fromTemplateUrl('/app/context_menu/new.filter.html', function (modal) {
            $scope.modal = modal;
        }, {
            animation: 'slide-in-up',
            focusFirstInput: true,
            scope: $scope
        });

        $ionicModal.fromTemplateUrl('/app/context_menu/repo.filter.html', function (modal) {
            $scope.repomodal = modal;
        }, {
            animation: 'slide-in-up',
            scope: $scope
        });

        $scope.closeModal = function () {
            $scope.modal.hide();
            $scope.flag.searchVisible = false;
        };

        $scope.didSearchToggleClick = function () {
            $scope.flag.searchVisible = !$scope.flag.searchVisible;
            $ionicTabsDelegate.select(2);
        }

        $scope.didFilterClick = function () {
            $scope.modal.show();
        }

        $scope.didRepoFilterClick = function () {
            $scope.repomodal.show();
        }

        $scope.didRepoSelectClick = function (repo) {
            $rootScope.websiteName = repo.name;
            $state.go('portal.website');
        }

        $scope.closeRepoModal = function () {
            $scope.repomodal.hide();
            $scope.data.searchState = "";
            $scope.flag.searchVisible = false;
        };

        $scope.didOtherTabsClick = function () {
            $ionicTabsDelegate.select(2);
        }

        $scope.didSearchIgnoreClick = function () {
            $scope.data.searchState.name = '';
            $scope.flag.searchVisible = false;
        }

        $scope.printme = function (data) {
            console.log(data);
        }
    }
]);
